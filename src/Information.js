import React from 'react';

import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';

import BusinessIcon from '@material-ui/icons/Business';
import LocationIcon from '@material-ui/icons/LocationOn';
import PhoneIcon from '@material-ui/icons/Phone';
import FaxIcon from '@material-ui/icons/Print';
import EmailIcon from '@material-ui/icons/Email';
import FacebookIcon from '@material-ui/icons/Facebook';

import 'fontsource-roboto';
import "./Contact.css"

function Contact() {
    return(
        <List>
            <ListItem>
                <ListItemAvatar>
                    <Avatar>
                        <BusinessIcon/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText>
                    <b>First Star Dental</b>
                    <br></br>
                    Yupei Chen, DDS, MS, PhD
                </ListItemText>
            </ListItem>
            <ListItem>
                <ListItemAvatar>
                    <Avatar>
                        <LocationIcon/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText>
                    <b>Address:</b>
                    <br></br>
                    2301 N. Central Expy, Suite 270, Plano, TX 75075
                </ListItemText>
            </ListItem>
            <ListItem>
                <ListItemAvatar>
                    <Avatar>
                        <PhoneIcon/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText>
                    <b>Phone:</b>
                    <br></br> 
                    (972) 479-9304
                </ListItemText>
            </ListItem>
            <ListItem>
                <ListItemAvatar>
                    <Avatar>
                        <FaxIcon/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText>
                    <b>Fax:</b>
                    <br></br>
                    (972) 491-7258
                </ListItemText>
            </ListItem>
            <ListItem>
                <ListItemAvatar>
                    <Avatar>
                        <EmailIcon/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText>
                    <b>Email:</b> 
                    <br></br>
                    firststardental@yahoo.com
                </ListItemText>
            </ListItem>
            <ListItem>
                <ListItemAvatar>
                    <Avatar>
                        <FacebookIcon/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText>
                    <b>Follow Us: </b>
                    <br></br>
                    <a href = "https://www.facebook.com/firststardental/">https://www.facebook.com/firststardental/</a>
                </ListItemText>
            </ListItem>
        </List>
    )
}

export default Contact;