import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Map from "./Map";
import Information from "./Information";
import Schedule from "./Schedule";
import Procedure from "./Procedure";
import Header from "./Header";
import Footer from "./Footer";
import "./Styles.css"
import Logo from "./logo.png";

function App() {

  return (
    <div className = "flexbox">
      <Header />
      <br></br>
      <br></br>
      <main>
        <Container>
          <Grid container spacing = {3} className = 'frame'>
            <Grid item xs={6}>
              <img src={Logo} className ="logo"/>
            </Grid>
            <Grid item xs={6}>
              
            </Grid>
          </Grid>
        </Container>
        <br></br>
        <br></br>
        <Procedure />
        <br></br>
        <br></br>
        <Container>
          <Grid container spacing={3} className ='frame'> 
            <Grid item xs={6}>
              <Schedule />
            </Grid>
            <Grid item xs={6}>
              <Information />
            </Grid>
            <Grid item xs={12}>
              <Map/>
            </Grid>
          </Grid>
          </Container>
      </main>
      <br></br>
      <br></br>
      <Footer />
    </div>
  );
}

export default App;
