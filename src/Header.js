import React from 'react';
import StarIcon from '@material-ui/icons/Star';

import "./Styles.css";

function Header() {
    return (
        <header>
            <nav>
                <div className = "page-header">
                    <a href = "http://localhost:3000/" className = "brand-logo">
                        FIRST STAR DENTAL
                    </a>
                </div>
            </nav>
        </header>
    )
}

export default Header;