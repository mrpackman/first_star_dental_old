import React from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

function Schedule() {
    return(
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>
                            <b>
                                Day
                            </b>
                        </TableCell>
                        <TableCell>
                            <b>
                                Hours
                            </b>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        <TableCell>Sunday</TableCell>
                        <TableCell>Closed</TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>Monday</TableCell>
                        <TableCell>9:30AM – 5:30PM</TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>Tuesday</TableCell>
                        <TableCell>9:30AM – 5:30PM</TableCell>
                    </TableRow>
                    <TableRow>    
                        <TableCell>Wednesday</TableCell>
                        <TableCell>Closed</TableCell>
                    </TableRow>
                    <TableRow>    
                        <TableCell>Thursday</TableCell>
                        <TableCell>Closed</TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>Friday</TableCell>
                        <TableCell>9:30AM – 5:30PM</TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>Saturday</TableCell>
                        <TableCell>9:30AM – 5:30PM</TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default Schedule;