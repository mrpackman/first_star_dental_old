import React from 'react';
import {
    GoogleMap,
    useLoadScript,
    Marker,
    InfoWindow
} from "@react-google-maps/api";
import mapStyle from "./mapStyle"

const libraries = ["places"];
const mapContainerStyle = {
    width: "100%",
    height: "33vh",
}
const center = {
    lat: 33.031061,
    lng: -96.708869
}
const options = {
    disableDefaultUI: true,
    zoomControl: true,
    fullscreenControl: true
}

function Map() {

    const {isLoaded, loadError} = useLoadScript({
        googleMapsApiKey: "AIzaSyBujHB845-ToJKlu_yW3WQPuWynQmdgEAY",
        libraries
    });

    if (loadError) return "Error loading maps";
    if (!isLoaded) return "Loading Maps"

    return (

        <div>
            <GoogleMap 
            mapContainerStyle = {mapContainerStyle} 
            zoom = {15}
            center = {center}
            options= {options}>
                <Marker 
                    position = {{lat : center.lat, lng: center.lng}}
                />
            </GoogleMap>
        </div>
    )
}

export default Map;