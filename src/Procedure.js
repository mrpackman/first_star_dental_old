import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';

import "./Styles.css"

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
}));

function Procedure() {

  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

    return(
      <Container>
          <Grid container spacing = {3} className ='frame'>
            <Grid item xs = {3} align = "center" >
              <Button variant="contained" className="button" disableElevation onClick={handleClick}>Dental Cleaning</Button>
              <Popover 
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'center',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'center',
                }}
              >
                The content of the Popover.
              </Popover>
            </Grid>
            <Grid item xs = {3} align = "center">
                <Button variant="contained" className="button" disableElevation>Dental Filling</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Dental Crown</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Dental Implant???</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Dental Bridge And Crown</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Dental Sealant</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Dental Bonding???</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Dental Veneer</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Denture</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Tooth Extraction</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Root Canal</Button>
            </Grid>
            <Grid item xs = {3} align = "center">
              <Button variant="contained" className="button" disableElevation>Inlay/Onlay???</Button>
            </Grid>
          </Grid> 
          </Container>
    )
}

export default Procedure